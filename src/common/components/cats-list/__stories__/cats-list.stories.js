import { CatsList } from '../cats-list';
import { MemoryRouter } from 'react-router-dom';
import React from 'react';

export default {
  title: 'CatsList',
  component: CatsList,
};

export const GetCatsList = () => (
  <MemoryRouter>
    <CatsList></CatsList>
  </MemoryRouter>
);
