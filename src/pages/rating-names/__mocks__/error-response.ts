export const errorResponse = {
  data: null,
  isBoom: true,
  isServer: true,
  output: {
    statusCode: 500,
    payload: {
      statusCode: 500,
      error: 'Internal Server Error',
    },
    headers: {},
  },
};
