import { test as base, expect } from '@playwright/test';
import { errorResponse } from '../__mocks__/error-response';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {
  await page.route(
    request => request.href.includes('/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        body: JSON.stringify(errorResponse),
      });
    }
  );
  await ratingPage.openRatingPage();
  await expect(page.locator(ratingPage.alertSelector)).toContainText(
    'Ошибка загрузки рейтинга'
  );
});

test('Рейтинг котиков отображается по убыванию', async ({
  page,
  ratingPage,
}) => {
  await ratingPage.openRatingPage();
  await ratingPage.checkRatingDisplayedInDescendingOrder();
});
