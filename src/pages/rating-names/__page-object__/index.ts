import { expect, Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

export class RatingPage {
  private page: Page;
  public alertSelector = '//*[contains(@class,"alertify-notifier")]';
  public firstPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 1]';
  public secondPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 2]';
  public thirdPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 3]';
  public fourthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 4]';
  public fifthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 5]';
  public sixthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 6]';
  public seventhPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 7]';
  public eighthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 8]';
  public ninthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 9]';
  public tenthPlaceSelector = '(//td[contains(@class, "rating-names_item-count__1LGDH")])[position() = 10]';

  private selectors = [
    this.firstPlaceSelector,
    this.secondPlaceSelector,
    this.thirdPlaceSelector,
    this.fourthPlaceSelector,
    this.fifthPlaceSelector,
    this.sixthPlaceSelector,
    this.seventhPlaceSelector,
    this.eighthPlaceSelector,
    this.ninthPlaceSelector,
    this.tenthPlaceSelector,
  ];

  constructor({
                page,
              }: {
    page: Page;
  }) {
    this.page = page;
  }

  async openRatingPage() {
    return await test.step('Открываю страницу с рейтингом', async () => {
      await this.page.goto('/rating')
    })
  }

  async getLikes(page: Page, selector: string): Promise<number> {
    const textContent = await page.textContent(selector);
    return parseInt(textContent || '0', 10);
  }

  async getAllLikes(page: Page): Promise<number[]> {

    const likes = [];
    for (const selector of this.selectors) {
      const like = await this.getLikes(page, selector);
      likes.push(like);
    }
    return likes;
  }

  checkLikesDescending(likes: number[]): void {
    for (let i = 0; i < likes.length - 1; i++) {
      if (likes[i] < likes[i + 1]) {
        throw new Error(`Ошибка: Рейтинг не по убыванию на позиции ${i + 1}. Значение ${likes[i]} меньше значения ${likes[i + 1]}`);
      }
      expect(likes[i]).toBeGreaterThanOrEqual(likes[i + 1]);
    }
  }

  async checkRatingDisplayedInDescendingOrder() {
    return await test.step('Проверка, что рейтинг отображается в порядке убывания', async () => {
      const likes = await this.getAllLikes(this.page);
      this.checkLikesDescending(likes);
    })
  }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
>;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};